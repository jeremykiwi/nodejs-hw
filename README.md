## Homework Information 

| 💻          | **Date**       | **Project Folder** | memo |
| :---:       |  :------:   |:---------      | :--------------------   |
| Homework 1  | 2019-10-29  | - [JeremyApp](/JeremyApp)      | |
| Homework 2  | 2019-11-12  | - [MyLineWebhook](/MyLineWebhook)  | |
| Homework 3  | 2019-11-19  | - [MyLineWebhook](/MyLineWebhook)  | add CommonLogger.js |
