## HW1 Note ☕️

### step 1 clone remote git repository
* GitLab remote server without supplying your username or password each time
  * https://docs.gitlab.com/ee/ssh/
```shell
cd /home/jeremykiwi/git/gitlab
git clone https://gitlab.com/jeremykiwi/nodejs-hw.git
```

### step 2 move your project folder in nodejs-hw folder and add .gitignore

* nodejs gitignore
  * https://www.gitignore.io/api/node

```shell
# move project folder
mv /home/jeremykiwi/JeremyApp /home/jeremykiwi/git/gitlab/nodejs-hw/JeremyApp

# create .gitignore file
touch .gitignore

# edit .gitignore file
nano .gitignore

# paste, save (control + o), and exit (control + x)

```

### step 3 git commit and push your project

```shell
git add .
git commit -m "HW1"
git push
```

