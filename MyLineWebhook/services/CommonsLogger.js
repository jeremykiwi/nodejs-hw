(function() {

    const SimpleNodeLogger = require('simple-node-logger');

    let logger = {
        init: function(fileName){
            const opts1 = {
                logDirectory: './logs',
                fileNamePattern: `${fileName}-<DATE>.log`,
                dateFormat: 'YYYY-MM-DD',
                timestampFormat: 'YYYY-MM-DD HH:mm:ss.SSS'
            };
            this.normalLogger = SimpleNodeLogger.createRollingFileLogger(opts1);
    
            const opts2 = {
                logDirectory: './logs',
                fileNamePattern: `${fileName}-error-<DATE>.log`,
                dateFormat: 'YYYY-MM-DD',
                timestampFormat: 'YYYY-MM-DD HH:mm:ss.SSS'
            };
            this.errorLogger = SimpleNodeLogger.createRollingFileLogger(opts2);
        },
        log: function(){
            let mylog = `${Object.values(arguments).join(' ')}`;
    
            this.normalLogger.info(mylog);
        },
        error: function(){
            let mylog = `${Object.values(arguments).join(' ')}`;
    
            this.errorLogger.error(mylog);
        }
    }
    logger.init('webhook');

    class CommonsLogger {
        constructor() {
            this.loggerFun = this.loggerFun.bind(this);
        }

        async loggerFun(text) {
            logger.log(text);
        }
    }

    module.exports = new CommonsLogger();
}());