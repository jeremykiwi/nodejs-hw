var express = require('express');
var router = express.Router();
var https = require('https');

const configs = require('../configs.js'); // config

const line = require('@line/bot-sdk'); // line api

const AssistantCtl = require('../Controller/AssistantController');
const MongodbCtl = require('../Controller/MongodbController');
const CommonLogger = require('../services/CommonsLogger');

MongodbCtl.init();

// const SimpleNodeLogger = require('simple-node-logger');

const channelAccessToken = configs.channelAccessToken;

const client = new line.Client({
    channelAccessToken: channelAccessToken
});



// function test() {
//   let initMongodb = await MongodbCtl.init();

//   let insertResult = await MongodbCtl.insertOne('testDB', 'testCollection', 'testDocument');
// }

// const MongoClient = require('mongodb').MongoClient;
// const uri = configs.mongodb;
// const Mclient = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
// Mclient.connect((err, db) => {
//   const collection = Mclient.db("test").collection("devices");
//   // perform actions on the collection object
//   if(!err) console.log('connect to db');

//   var dbo = db.db("runoob");
//   var myobj = { name: "菜鸟教程", url: "www.runoob" };
//   dbo.collection("app").insertOne(myobj, function(err, res) {
//     if (err) throw err;
//       console.log("文档插入成功");
//       db.close();
//     });

//   Mclient.close();
// });



// watson assistant
// const AssistantV2 = require('ibm-watson/assistant/v2');
// const { IamAuthenticator } = require('ibm-watson/auth');

// const service = new AssistantV2({
//   version: configs.assistant.version,
//   authenticator: new IamAuthenticator({
//     apikey: configs.assistant.apikey,
//   }),
//   url: configs.assistant.url,
// });

// let lineUserSessions = {
//   "userId": "sessionId"
// };

router.post('/line', async function(req, res, next) {
    // 印出 webhook 傳入內容
	// console.log(JSON.stringify(req.body, null, 2));
  
  res.status(200).send();
  
  try {
    let targetEvent = req.body.events[0];
    
    if(targetEvent.type == 'message'){
      if(targetEvent.message.type == 'text') {
        let userId = targetEvent.source.userId;
        let userSay = targetEvent.message.text;

        CommonLogger.loggerFun('userId: ' + userId);
        CommonLogger.loggerFun('userSay: ' + userSay);

        let newTextMessage = await AssistantCtl.sendMessage(userId, userSay);

        if(newTextMessage.success) {
          let document = {
            userId: userId,
            userSay: userSay,
            returnMessage: newTextMessage.message.text,
            timestamp: Date.now()
          }
  
          MongodbCtl.insertOne('LineDate', 'chatLog', document);

          replyToLine(targetEvent.replyToken, newTextMessage.message);
        } else {
          throw new Excption(newTextMessage.message);
        }

        // console.log('newTextMessage: ', newTextMessage);

        

        

      }
    }
  } catch(err) {

    let replyToken = req.body.events[0].replyToken;

    let errMsg = {
      type: 'text',
      text: '很抱歉, 目前系統維護中~'
    }

    replyToLine(replyToken, errMsg);

    CommonLogger.loggerFun('XXXX')
  }
});

module.exports = router;

// line sdk
function replyToLine(rplyToken, messages) {
  return client.replyMessage(rplyToken, messages)
  .then((result) => {
    console.log('result: ', result);
    return true;
  })
  .catch((err) => {
    // error handling
    console.log('error:', err);
    return false;
  });
}

// let AssistantController = {

// }

// old process message
// function replyToLine(rplyToken, messages) {
// 	let rplyObj = {
// 		replyToken: rplyToken,
// 		messages: [messages]
// 	}
//     let rplyJson = JSON.stringify(rplyObj);
// 	let options = setOptions();
//     let request = https.request(options, function (response) {
//         console.log('Status: ' + response.statusCode);
//         response.setEncoding('utf8');
//         response.on('data', function (body) {
//             console.log('body:', body);
//         });
//     });
//     request.on('error', function (e) {
//         console.log('Request error:', e.message);
//     })
//     request.end(rplyJson);
// }

// function setOptions() {
// 	let ChannelAccessKey = 'seXKGtkTN5NMDWw23QTl6yzEATRFM/P8scWOHAIeIpWLoqbNTi7ENxWUjJDnC9CAfhuaes+Thk8+gt1cS92cLGdLMHCuDHXmk2YrKkTC1sbXaJ0eB0g4CKowHSEGqR9gKKJK2asxzi0vIOZ1+4kOcgdB04t89/1O/w1cDnyilFU=';
//     let option = {
//         host: 'api.line.me',
//         port: 443,
//         path: '/v2/bot/message/reply',
//         method: 'POST',
//         headers: {
//             'Content-Type': 'application/json',
//             'Authorization': 'Bearer ' + ChannelAccessKey
//         }
//     }
//     return option;
// }

// assistant api v2 控制物件
// let AssistantController = {
// 	init: function(){
// 		this.service = new AssistantV2({
// 			version: '2019-02-28',
// 			authenticator: new IamAuthenticator({
// 			    apikey: configs.assistant.apikey,
// 			}),
// 			url: configs.assistant.url,
// 		});
// 	},
// 	createSession: function(){
// 		return this.service.createSession({
// 			assistantId: configs.assistant.assistantId
// 		}).then(res => {
// 			return res.result;
// 		}).catch(err => {
// 			console.log('createSession', err);
// 			return err.body;
// 		});
// 	},
// 	deleteSession: function(session_id){
// 		return this.service.deleteSession({
// 			assistantId: configs.assistant.assistantId,
// 			sessionId: session_id,
// 		}).then(res => {
// 			return res.result;
// 		}).catch(err => {
// 			console.log('deleteSession', err);
// 			return err.body;
// 		});
// 	},
// 	message: function(text, session_id){
// 		return this.service.message({
// 			assistantId: configs.assistant.assistantId,
// 			sessionId: session_id,
// 			input: {
// 				'message_type': 'text',
// 				'text': text
// 			}
// 		})
// 		.then(res => {
// 			// console.log(JSON.stringify(res, null, 2));
// 			return res.result;
// 		}).catch(err => {
// 			console.log('message', err);
// 			return err.body;
// 		});
// 	}
// }
// AssistantController.init();

// let logger = {
// 	init: function(fileName){
// 		const opts1 = {
// 			logDirectory: './logs',
// 			fileNamePattern: `${fileName}-<DATE>.log`,
// 			dateFormat: 'YYYY-MM-DD',
// 			timestampFormat: 'YYYY-MM-DD HH:mm:ss.SSS'
// 		};
// 		this.normalLogger = SimpleNodeLogger.createRollingFileLogger(opts1);

// 		const opts2 = {
// 			logDirectory: './logs',
// 			fileNamePattern: `${fileName}-error-<DATE>.log`,
// 			dateFormat: 'YYYY-MM-DD',
// 			timestampFormat: 'YYYY-MM-DD HH:mm:ss.SSS'
// 		};
// 		this.errorLogger = SimpleNodeLogger.createRollingFileLogger(opts2);
// 	},
// 	log: function(){
// 		let mylog = `${Object.values(arguments).join(' ')}`;

// 		this.normalLogger.info(mylog);
// 	},
// 	error: function(){
// 		let mylog = `${Object.values(arguments).join(' ')}`;

// 		this.errorLogger.error(mylog);
// 	}
// }
// logger.init('webhook');