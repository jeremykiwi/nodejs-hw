var express = require('express')
var router = express.Router();

const MongodbCtl = require('../Controller/MongodbController');
MongodbCtl.init();

const AssistantCtl = require('../Controller/AssistantController');

/**
 * 
 * 
 *  */
router.post('/db/find', async function(req, res, next){
    console.log(JSON.stringify(req.body, null, 2));
    
    let params = req.body,
    dbName = '',
    collection = '',
    query = {}
    

    // let findResult = await MongodbCtl.find('LineDate', 'chatLog', {});
    dbName = 'LineDate'; 
    collection = 'chatLog';
    query = {
        timestamp:{
            $gt: params.sTime - 0,
            $lt: params.eTime - 0
        }
    }

    MongodbCtl.find(dbName, collection, {}).then(result => {
        console.log(result);

        res.status(200).send(result);
    })
    .catch(err=>{
        throw new Error(`fail to find data in ${dbName}.${collection}`);
    })

    console.log(findResult);


    res.status(200).send();
});


router.post('/chat', async function(req, res, next){
    console.log(req.body);

    let params = req.body;

    let assistantReturn = await AssistantCtl.sendMessage(params.userId, params.userSay);

    res.status(200).send({
        success: true,
        message: assistantReturn
    });

});

module.exports = router;