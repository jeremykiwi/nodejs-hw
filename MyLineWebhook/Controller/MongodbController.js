const configs = require('../configs');

const MongoClient = require('mongodb').MongoClient;
const url = configs.mongodb;

class MongodbController {
    constructor() {
        this.init = this.init.bind(this);
        this.insertOne = this.insertOne.bind(this);
        this.find = this.find.bind(this);
    }

    init() {
        let _this = this;
        return new Promise((resolve) => {
            MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
                if (err) {
                    console.log('connect error:', err);
                } else {
                    console.log('success connect to db');
                    _this.db = db;
                }
                
                return err || 'success';
            });
        });
    }

    insertOne(dbName, collection, document) {
        let dbo = this.db.db(dbName);
        dbo.collection(collection).insertOne(document, function(err, res) {
            if(err) {

            } else {
                console.log()
            }

            return err || 'success';
        })
    }

    find(dbName, collection, nosqlquery) {
        return new Promise((resolve, reject) => {
            
            let dbo = this.db.db(dbName);

            dbo.collection(collection).find(nosqlquery).toArray(function(err, result) {
                if(err) {
                    reject({
                        success: false,
                        error: err
                    })
                } else {
                    resolve({
                        success: true,
                        document: result
                    })
                }
            });
            
        }); 
    }
}

module.exports = new MongodbController();
