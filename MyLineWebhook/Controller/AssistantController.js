(function(){
    const configs = require('../configs');
    const assistantAPI = require('../services/AssistantAPIV2');

    let userSessions = {}; // save sdk session

    class AssistantController {
        constructor() {
            assistantAPI.init(configs.assistant);
            this.sendMessage = this.sendMessage.bind(this);
        }

        async sendMessage(userId, userSay) {
            let userSession = userSessions.userId;

            if(!userSession) {
                let newSession = await assistantAPI.createSession();
                userSession = newSession.session_id;
                userSessions[userId] = userSession;
            }

            // let assistantAns = await assistantAPI.message(userSay, userSession);
            
            // console.log('assistantAns: ', assistantAns);

            // let responseContent = assistantAns.output.generic[0].text;

            return assistantAPI.message(userSay, userSession)
            .then((result) => {
                // JSON.parse(result.output.generic[0].text);
                

                console.log(result.output.generic[0].text);
                // let oo = JSON.parse(result.output.generic[0].text);
                // console.log(result);

                // let r = {
                //     type: 'text',
                //     text: result.output.generic[0].text
                // }

                // if(oo.answerId == 'A1') {
                //     r = 
                // } 

                return {
                    success: true,
                    result: result,
                    message: JSON.parse(result.output.generic[0].text)
                }
            }).catch(() => {
                return {
                    success: false,
                    message: 'fail to call assistant'
                }
            });


            // return {
            //     type: 'text',
            //     text: responseContent
            // }
        }
    }
    
    module.exports = new AssistantController();
}());