### Line Doc
- https://developers.line.biz/en/docs/


### Line API message-objects
 - https://developers.line.biz/en/reference/messaging-api/#message-objects


### Line API getProfile
 - https://developers.line.biz/en/reference/messaging-api/#get-profile

### ngrok
- https://ngrok.com/

```shell
# start ngrok
./ngrok http 3000
```

### MongoDB nodejs api
 - http://mongodb.github.io/node-mongodb-native/2.0/

